<?php


/**
 * hook_perm
 */
function anonymizer_perm() {
  return array('post anonymous comments');
}

/**
 * Check to see if a comment is anonymous
 * @returns TRUE if comment is marked anonymous, false otherwise.
 */
function anonymizer_comment_is_anon($cid) {
  $query = db_query('SELECT COUNT(*) FROM {anonymizer} WHERE cid = %d', $cid);
  return (db_result($query) == 0) ? FALSE : TRUE;
}

/**
 * hook_form_alter
 */
function anonymizer_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'comment_form') {
    if (!user_access('post anonymous comments') || empty($form['author']))
      return;
    $form['anonymous'] = array(
      '#type' => 'checkbox',
      '#title' => 'Post anonymously',
      '#weight' => '1',
    );
    $form['comment_filter']['#weight'] = 2;
  }
}

/**
 * hook_comment
 */
function anonymizer_comment($comment, $op) {
  if ($op == 'insert' || $op == 'update') {
    $anon = anonymizer_comment_is_anon($comment['cid']);
    if ($comment['anonymous'] == TRUE && $anon == 0) {
      db_query('INSERT INTO {anonymizer} (cid) VALUES (%d)', $comment['cid']);
    }
    elseif ($comment['anonymous'] == FALSE && $anon != 0) {
      db_query('DELETE FROM {anonymizer} WHERE cid = %d', $comment['cid']);
    }
  }
  elseif ($op == 'delete') {
    if (anonymizer_comment_is_anon($comment->cid))
      db_query('DELETE FROM {anonymizer} WHERE cid = %d', $comment->cid);
  }
}

function anonymizer_preprocess_comment_folded(&$variables) {
  _anonymize_comment($variables);
}

function anonymizer_preprocess_comment(&$variables) {
  _anonymize_comment($variables);
}

/**
 * Make the comment anonymous by hiding the user's name.
 */
function _anonymize_comment(&$variables) {
  $comment = $variables['comment'];

  if (anonymizer_comment_is_anon($comment->cid) == FALSE)
    return;

  unset($comment->name);
  unset($comment->uid);
  $variables['author'] = theme('username', $comment);
  $variables['signature'] = '';
  $variables['submitted'] = t('Submitted anonymously on @datetime.',
    array(
      '@datetime' => format_date($comment->timestamp)
    )); 
}
